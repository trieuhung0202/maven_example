package com.example;
import java.util.Scanner;

import com.example.service.UIManager;

public class App {
    public static void main(String[] args) {
        UIManager uiManager = new UIManager();
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.println("Menu:");
            System.out.println("    1.Enter recruiting amount:");
            System.out.println("    2.Add new students");
            System.out.println("    3.Display all students");
            System.out.println("    4.Display all new employees");
            System.out.println("    5.Exit");
            System.out.println("Your choise is: ");

            int option = Integer.parseInt(scanner.nextLine().trim());

            switch(option) {
                case 1:
                    uiManager.enterRecruitingAmount(scanner); 
                    break;

                case 2:
                    uiManager.enterNewStudent(scanner);
                    System.out.println("Add new students successfull !!!");
                    break;

                case 3:
                    System.out.println("\nAll students have just added are:");
                    uiManager.displayAllStudent();
                    break;

                case 4:
                    System.out.println("\nOur new employee are:");
                    uiManager.displayAllNewEmployee();
                    break;

                case 5:
                    scanner.close();
                    return;
            }
        }

    }   
}
