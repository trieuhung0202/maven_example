package com.example.Comparetions;

import java.util.Comparator;

import com.example.entities.GoodStudent;

public class GoodStudentComparator implements Comparator<GoodStudent>{

    @Override
    public int compare(GoodStudent goodStudent1, GoodStudent goodStudent2) {
        // TODO Auto-generated method stub
        if (Float.valueOf(goodStudent1.getGpa()).equals(Float.valueOf(goodStudent2.getGpa()))) {
            return (goodStudent1.getFullName().compareTo(goodStudent2.getFullName()));
        }
        return (Float.valueOf(goodStudent1.getGpa()).compareTo(Float.valueOf(goodStudent2.getGpa())));
    }
    
}
