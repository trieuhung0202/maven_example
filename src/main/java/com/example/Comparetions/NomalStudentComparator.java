package com.example.Comparetions;

import java.util.Comparator;

import com.example.entities.NomalStudent;

public class NomalStudentComparator implements Comparator<NomalStudent>{

    @Override
    public int compare(NomalStudent nomalStudent1, NomalStudent nomalStudent2) {
        // TODO Auto-generated method stub
        if (Float.valueOf(nomalStudent1.getEntryTestScore()).equals(Float.valueOf(nomalStudent2.getEntryTestScore()))) {
            return (Float.valueOf(nomalStudent1.getEnglishScore()).compareTo(Float.valueOf(nomalStudent2.getEnglishScore())));
        }

        return (Float.valueOf(nomalStudent1.getEntryTestScore()).compareTo(Float.valueOf(nomalStudent2.getEntryTestScore())));
    }
    
}
