package com.example.Comparetions;

import java.util.Comparator;

import com.example.entities.Student;

public class StudentComparator implements Comparator<Student>{

    @Override
    public int compare(Student student1, Student student2) {
        // TODO Auto-generated method stub
        if (student1.getFullName().equals(student2.getFullName())) {
            return (student1.getPhoneNumber().compareTo(student1.getPhoneNumber()));
        }
        return (student1.getFullName().compareTo(student2.getFullName()));
    }
    
}
