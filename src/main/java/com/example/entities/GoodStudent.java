package com.example.entities;

public class GoodStudent extends Student{
    private float gpa;
    private String bestRewardName;

    public GoodStudent() {}

    public GoodStudent (String fullName, String doB, String sex, String phoneNumber, String universityName, String gradeLevel, String gpa, String bestRewardName) {
        super(fullName, doB ,sex,phoneNumber,universityName,gradeLevel);
        this.gpa = Float.valueOf(gpa);
        this.bestRewardName = bestRewardName;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return ("Full name: " + fullName + "\n" +
                "Birth day: " + doB + "\n" +
                "Sex: " + sex + "\n" +
                "Phone number: " + phoneNumber + "\n" +
                "University name: " + universityName + "\n" +
                "Grade level: " + gradeLevel + "\n" +
                "GPA: " + gpa + "\n" +
                "Best reward name: " + bestRewardName + "\n" +
                "-----------------------------------\n");
    }

    @Override
    public void showMyInfo() {
        // TODO Auto-generated method stub
        System.out.println(this);
    }

}
