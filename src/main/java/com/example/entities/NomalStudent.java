package com.example.entities;

public class NomalStudent extends Student{
    private float englishScore;
    private float entryTestScore;


    public NomalStudent(String fullName, String doB, String sex, String phoneNumber, String universityName, String gradeLevel, String englishScore, String entryTestScore) {
        super(fullName, doB, sex, phoneNumber, universityName, gradeLevel);
        this.englishScore = Float.parseFloat(englishScore.trim());
        this.entryTestScore = Float.parseFloat(entryTestScore.trim());
    }

    @Override
    public String toString() {
        return ("Full name: " + fullName + "\n" +
                "Birth day: " + doB + "\n" +
                "Sex: " + sex + "\n" +
                "Phone number: " + phoneNumber + "\n" +
                "University name: " + universityName + "\n" +
                "Grade level: " + gradeLevel + "\n" +
                "English score: " + englishScore + "\n" +
                "Entry test score: " + entryTestScore + "\n" +
                "-----------------------------------\n");
    }

    public float getEnglishScore() {
        return englishScore;
    }

    public void setEnglishScore(float englishScore) {
        this.englishScore = englishScore;
    }

    public float getEntryTestScore() {
        return entryTestScore;
    }

    public void setEntryTestScore(float entryTestScore) {
        this.entryTestScore = entryTestScore;
    }

    @Override
    public void showMyInfo() {
        // TODO Auto-generated method stub
        System.out.println(this);
        
    }
}
