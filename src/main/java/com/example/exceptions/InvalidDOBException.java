package com.example.exceptions;

public class InvalidDOBException extends Exception{
    public InvalidDOBException(String errorMessage) {
        super(errorMessage);
    }
}
