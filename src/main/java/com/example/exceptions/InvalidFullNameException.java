package com.example.exceptions;

public class InvalidFullNameException extends Exception{
    public InvalidFullNameException(String errorMessage) {
        super(errorMessage);
    }
}
