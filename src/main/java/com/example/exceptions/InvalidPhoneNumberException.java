package com.example.exceptions;

public class InvalidPhoneNumberException extends Exception{
    public InvalidPhoneNumberException(String errorMessage) {
        super(errorMessage);
    }
}
