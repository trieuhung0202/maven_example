package com.example.service;

import java.util.ArrayList;
import java.util.Collections;


import com.example.Comparetions.GoodStudentComparator;
import com.example.Comparetions.NomalStudentComparator;
import com.example.entities.GoodStudent;
import com.example.entities.NomalStudent;
import com.example.entities.Student;

public class Recruitor {

    private int amount;

    public static ArrayList<Student> students = new ArrayList<Student>();
    static {
        students.add(new GoodStudent("Nguyen Van A", "01/01/2001", "Male", "0901234567", "PTIT", "Good", "3.9", "chicken blood cutting"));
        students.add(new GoodStudent("Nguyen Van B", "01/01/2001", "Male", "0901234567", "PTIT", "Good", "4.0", "duck blood cutting"));
        students.add(new GoodStudent("Nguyen Van C", "01/01/2001", "Male", "0901234567", "PTIT", "Good", "3.9", "chicken blood cutting"));
        students.add(new NomalStudent("Tran Van D", "01/02/2001", "Female", "0981236547", "PTIT", "Nomal", "900", "1"));
        students.add(new NomalStudent("Tran Van E", "01/02/2001", "Female", "0981236547", "PTIT", "Nomal", "800", "2"));
        students.add(new NomalStudent("Tran Van F", "01/02/2001", "Female", "0981236547", "PTIT", "Nomal", "900", "1"));
    }

    public Recruitor () {}

    public Recruitor (int amount) {
        this.amount = amount;
    }
    
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getGoodAmount() {
        int count = 0;
        for (Student student : students) {
            if (student instanceof GoodStudent) {
                count ++;
            }
        }
        return count;
    }

    public int getNomalAmount() {
        int count = 0;
        for (Student student : students) {
            if (student instanceof NomalStudent) {
                count ++;
            }
        }        
        return count;
    }

    public void add(Student student) {
        students.add(student);
    }

    public ArrayList<Student> recruit(){
        ArrayList<Student> newEmployees = new ArrayList<Student>();   
        ArrayList<GoodStudent> goodStudents = new ArrayList<GoodStudent>();
        ArrayList<NomalStudent> nomalStudents = new ArrayList<NomalStudent>();

        for (Student student : students) {
            if (student instanceof GoodStudent) {
                goodStudents.add((GoodStudent) student);
            } else {
                nomalStudents.add((NomalStudent)student);
            }
        }

        Collections.sort(goodStudents, new GoodStudentComparator());
        Collections.sort(nomalStudents, new NomalStudentComparator());

        if (getGoodAmount() >= getAmount()) {
            for (int i = 0; i < amount; i++) {
                newEmployees.add(goodStudents.get(i));
            }
        } else {
            for (int i = 0; i < goodStudents.size(); i++) {
                newEmployees.add(goodStudents.get(i));
            }
            for (int i = 0; i < (getAmount() - goodStudents.size()); i++) {
                newEmployees.add(nomalStudents.get(i));
            }
        }
        return newEmployees;
    } 

}
