package com.example.service;

import java.util.Collections;
import java.util.Scanner;

import com.example.Comparetions.StudentComparator;
import com.example.entities.GoodStudent;
import com.example.entities.NomalStudent;
import com.example.exceptions.InvalidDOBException;
import com.example.exceptions.InvalidFullNameException;
import com.example.exceptions.InvalidPhoneNumberException;

public class UIManager {

    public static final int START_COUNT_VALUE = 1;

    Recruitor recruitor = new Recruitor();
    public static String inputName (Scanner scanner) {
        String result;
        try {
            System.out.print("Full name: ");
            result = scanner.nextLine();
            DataValidator dataValidator = new DataValidator();
            dataValidator.nameCheck(result);
        } catch ( InvalidFullNameException e) {
            //TODO: handle exception
            System.out.println(e.getMessage());;
            return inputName(scanner);
        }
        return result;
    }

    public static String inputBirthday (Scanner scanner) {
        String result;
        try {
            System.out.print("Birthday: ");
            result = scanner.nextLine();
            DataValidator dataValidator = new DataValidator();
            dataValidator.birthdayCheck(result);
        } catch (InvalidDOBException e) {
            //TODO: handle exception
            System.out.println(e.getMessage());;
            return inputBirthday(scanner);
        }
        return result;
    }

    public static String inputPhone (Scanner scanner) {
        String result;
        try {
            System.out.print("Phone Number: ");
            result = scanner.nextLine();
            DataValidator dataValidator = new DataValidator();
            dataValidator.phoneCheck(result.trim());
        } catch (InvalidPhoneNumberException e) {
            //TODO: handle exception
            System.out.println(e.getMessage());;
            return inputPhone(scanner);
        }
        return result;
    }

    public void enterRecruitingAmount (Scanner scanner) {
        System.out.print("Enter recruiting amout: ");
        recruitor.setAmount(Integer.parseInt(scanner.nextLine().trim()));
    }

    public void enterNewStudent (Scanner scanner) {
        String continuee = "y";
        int count = START_COUNT_VALUE;
        while (continuee.equals("y")) {
            System.out.println("\nStudent " + count + ":");

            String fullName = inputName(scanner);

            String doB = inputBirthday(scanner);

            System.out.print("Gender: ");
            String sex = scanner.nextLine();

            String phoneNumber = inputPhone(scanner);

            System.out.println("University name: ");
            String universityName = scanner.nextLine();

            System.out.println("Grade level: ");
            String gradeLevel = scanner.nextLine();

            if (gradeLevel.toLowerCase().equals("good")) {
                System.out.print("GPA");
                String gpa = scanner.nextLine();

                System.out.print("Best reward name: ");
                String bestRewardName = scanner.nextLine();

                recruitor.add(new GoodStudent(fullName, doB, sex, phoneNumber, universityName, gradeLevel, gpa, bestRewardName));

            } else {
                System.out.print("English score: ");
                String englishScore = scanner.nextLine();

                System.out.print("Entry test score: ");
                String entryTestScore = scanner.nextLine();

                recruitor.add(new NomalStudent(fullName, doB, sex, phoneNumber, universityName, gradeLevel, englishScore, entryTestScore));
            }

            System.out.print("Do you want to continue?  (y/n)  ");
            continuee = scanner.nextLine().trim();
        }
    }

    public void displayAllStudent(){
        Collections.sort(recruitor.students, new StudentComparator());
        System.out.println(recruitor.students);
    }

    public void displayAllNewEmployee() {
        System.out.println(recruitor.recruit());
    }

}

